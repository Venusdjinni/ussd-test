package UssdTest;

import org.apache.log4j.BasicConfigurator;
import org.json.JSONObject;
import org.smslib.*;
import org.smslib.helper.CommPortIdentifier;
import org.smslib.modem.SerialModemGateway;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static UssdTest.MainClass.*;


public class SendMoney extends Thread {
    private String status = "start";
    public static final String ANSI_BRIGHT_RED = "\u001B[91m";
    public static final String ANSI_RESET = "\u001B[0m";
    private Service service;
    private List<String> parameters;
    private Dialer dialer;
    private InboundSMSNotification smsNotification;
    private OnSendMoneyFinish finish;

    SendMoney(List<String> parameters, OnSendMoneyFinish finish) {
        this.parameters = parameters;
        this.finish = finish;
    }

    public void run() {
        status = "start";
        MainClass.writeStatus(status, FILE_STATUS_PATH);
        sendMoney();
    }

    private void sendMoney() {
        String provider = parameters.get(0);
        String receiver = parameters.get(1);
        String amount   = parameters.get(2);
        String pin_code = parameters.get(3);

        JSONObject o = new JSONObject();
        o.put("provider", provider);
        o.put("receiver", receiver);
        o.put("amount", amount);
        o.put("pin_code", pin_code);
        System.out.println("received data: " + o.toString());
        status = "starting";
        MainClass.writeStatus(status, FILE_STATUS_PATH);

        final String ID = "modem.com1";

        try{
            // active le système de log de smslib
            BasicConfigurator.configure();
            service = Service.getInstance();
            service.S.SERIAL_POLLING = true;
            service.S.SERIAL_TIMEOUT = 5000;

            System.out.println("+++++++++");
            String comPort = null;
            Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
            while (thePorts.hasMoreElements()) {
                CommPortIdentifier com = (CommPortIdentifier) thePorts.nextElement();
                System.out.println(com.getName() + " : " + com.getPortType());
                comPort = com.getName();
            }
            System.out.println("+++++++++");

            if (comPort == null) {
                try {

                    File file = new File(FILE_COMPORT_PATH);
                    Scanner text = new Scanner(file);

                    comPort = text.nextLine();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    comPort = "/dev/ttyUSB0";
                    System.out.println("using default com port address: " + comPort);
                }
            }

            SerialModemGateway gateway = new SerialModemGateway(
                    ID,
                    comPort,
                    9600,
                    "Huawei",
                    "E160"
            );
            //IPModemGateway gateway = new IPModemGateway(ID, "", 33455, "manu", "model");
            gateway.setProtocol(AGateway.Protocols.PDU);
            gateway.setInbound(true);
            gateway.setOutbound(true);
            service.addGateway(gateway);

            dialer = new Dialer(gateway, buildDialList(provider, receiver, amount, pin_code));
            service.setUSSDNotification(dialer);
            smsNotification = new InboundSMSNotification();
            //service.setInboundMessageNotification(smsNotification);
            service.startService();

            status = "ussd_request";
            MainClass.writeStatus(status, FILE_STATUS_PATH);

            boolean status = dialer.start().join();
            if (status)
                System.out.println(">>> log 0: Operation successfully runned");
            // pas besoin parce que ce n'est pas un bug, donc l'explication a déjà été donnée avant
            //else System.err.println(">>> error 500: An error occured");
            //if (status)
                //smsNotification.completion.join();
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println(">>> error 500: " + exception.getMessage());
        } finally {
            stopService();
        }
    }

    private void stopService() {
        if (service != null && !service.getServiceStatus().equals(Service.ServiceStatus.STOPPED)) {
            try {
                service.stopService();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        status = "end";
        MainClass.writeStatus(status, FILE_STATUS_PATH);
        if (finish != null)
            finish.onFinish();
    }

    private List<String> buildDialList(String provider, String receiver, String amount, String pin_code) {
        ArrayList<String> dial = new ArrayList<>();

        if(provider.toLowerCase().equals("test")){
            dial.add(0,"*126#");
            dial.add(1,"7") ;
            dial.add(2,"1") ;
            dial.add(3,"1") ;
            dial.add(4,pin_code) ;
        }

        if(provider.toLowerCase().equals("orange")){
            dial.add("#149#");
            dial.add("1");
            dial.add(amount);
            dial.add(receiver);
            dial.add(pin_code);


            /*dial.add(0,"#149#");
            dial.add(1,"1") ;
            dial.add(2,"1") ;
            dial.add(3,receiver) ;
            dial.add(4,amount) ;
            dial.add(5,pin_code) ;*/
        }

        if(provider.toLowerCase().equals("mtn")){
            dial.add("*126#");
            dial.add("1") ;
            //dial.add("1") ;
            dial.add(receiver) ;
            dial.add(amount) ;
            //dial.add("KAMIX") ;
            dial.add(pin_code) ;
        }

        return dial;
    }

    void stopThread() {
        if (dialer != null)
            dialer.completion.complete(false);
        System.out.println(">>> error 300: Cancelled by route");
        /*if (smsNotification != null)
            smsNotification.completion.complete(false);*/
        stopService();
    }

    class Dialer implements IUSSDNotification {
        private AGateway gateway;
        private List<String> dial;
        private int step = 0;
        CompletableFuture<Boolean> completion;

        Dialer(AGateway gateway, List<String> dial) {
            this.gateway = gateway;
            this.dial = dial;
            completion = new CompletableFuture<>();
        }

        CompletableFuture<Boolean> start() {
            nextStep();
            return completion;
        }

        @Override
        public void process(AGateway aGateway, USSDResponse ussdResponse) {
            System.out.println(ussdResponse.getContent());
            if (step == dial.size() - 1) {
                // dernière étape
                // update du statut
                status = "finished";
                MainClass.writeStatus(status, FILE_STATUS_PATH);

                // ecriture
                System.out.println("Verifying ending string...\n");
                boolean result = verifySendingText(ussdResponse.getContent());

                // fin du dialer
                completion.complete(result);
            } else {
                if (!ussdResponse.getSessionStatus().equals(USSDSessionStatus.FURTHER_ACTION_REQUIRED)) {
                    // une erreur est survenue
                    System.err.println(">>> error 100: erreur de succession ussd at step " + step + ". Steps were " + String.join("*", dial) + "#");
                    completion.complete(false);
                } else {
                    try {
                        waitRandom();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    step++;
                    nextStep();
                }
            }
        }

        private void waitRandom() throws InterruptedException {
            Random rand = new Random();
            int wait = rand.nextInt(500) + 1500 ;
            TimeUnit.MILLISECONDS.sleep(wait);
        }

        private void nextStep() {
            try {
                USSDRequest request = new USSDRequest(dial.get(step));
                System.out.println("command: " + request.getRawRequest());
                boolean sent = service.sendUSSDRequest(request, gateway.getGatewayId());

                if (!sent)
                    throw new Exception("USSD Request not sent: " + request.getRawRequest());
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(">>> error 101: " + e.getMessage());
                completion.complete(false);
            }
        }
    }

    class InboundSMSNotification implements IInboundMessageNotification {
        CompletableFuture<Boolean> completion = new CompletableFuture<>();

        @Override
        public void process(AGateway aGateway, Message.MessageTypes messageTypes, InboundMessage inboundMessage) {
            System.out.print("A NEW SMS: ");
            System.out.println(inboundMessage.getText());
            completion.complete(true);
        }
    }

    private boolean verifySendingText(String text) {
        try {
            switch (parameters.get(0)) {
                case PROVIDER_ORANGE: return text.equals("Depot effectue. Vous allez recevoir les details de la transaction par SMS. Merci d avoir utilise le service Orange Money.");
                /*case PROVIDER_MTN:
                    //"Depot effectue avec succes à ARNOLD LOIC WABO OUOKAM (237681196283), Montant: 500FCFA, Nouveau solde: 8,500FCFA"
                    int fcfa = text.indexOf("FCFA") - 1;
                    boolean ind2 = text.substring(text.substring(0, fcfa).lastIndexOf(" ") + 1, fcfa).equals(parameters.get(2));
                    int parenth = text.indexOf("(");
                    boolean ind3 = text.substring(parenth + 4, text.indexOf(")")).equals(parameters.get(1));

                    boolean result = ind2 && ind3;
                    if (!result)
                        System.out.println(">>> log 400: verification string is incorrect");

                    return result;*/

                case PROVIDER_MTN: return text.contains("Depot effectue avec succes");
                case PROVIDER_TEST: return true;
                default: throw new Exception();
            }

            /*boolean ind1 = text.contains("Successful");
            int fcfa = text.indexOf("FCFA") - 1;
            boolean ind2 = text.substring(text.substring(0, fcfa).lastIndexOf(" ") + 1, fcfa).equals(parameters.get(2));
            int parenth = text.indexOf("(");
            boolean ind3 = text.substring(parenth + 4, text.indexOf(")")).equals(parameters.get(1));

            boolean result = ind1 && ind2 && ind3;

            if (!result)
                System.out.println(">>> log 400: verification string is incorrect");

            return result;*/
        } catch (Exception e) {
            System.err.print(">>> error 401: verification string in bad format: " + e.getMessage());
            return false;
        }
    }

    public interface OnSendMoneyFinish {
        void onFinish();
    }
}

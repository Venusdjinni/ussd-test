package UssdTest;

import java.io.IOException;
import java.util.Scanner;


public class CancelTransaction extends Thread {
    private ICancelTransaction cancelTransaction;
    private Scanner scanner;
    private boolean shouldStop = false;

    public CancelTransaction(ICancelTransaction cancelTransaction) {
        this.cancelTransaction = cancelTransaction;
    }

    private void waitForCancel() {
        scanner = new Scanner(System.in);
        while (!shouldStop) {
            try {
                if (hasNextLine()) {
                    String text = scanner.nextLine();
                    if (text.equals("cancel")) {
                        System.out.println("cancelling service");
                        // on arrête le thread SendMoney
                        cancelTransaction.cancelTransaction();
                        // on arrête également ce thread
                        shouldStop = true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean hasNextLine() throws IOException {
        while (System.in.available() == 0) {
            // [variant 1
            try {
                sleep(10);
            } catch (InterruptedException e) {
                return false;
            }// ]

            // [variant 2 - without sleep you get a busy wait which may load your cpu
            //if (this.isInterrupted()) {
            //    System.out.println("Thread is interrupted.. breaking from loop");
            //    return false;
            //}// ]
        }
        return scanner.hasNextLine();
    }

    public void setShouldStop(boolean shouldStop) {
        this.shouldStop = shouldStop;
    }

    public void run() {
        //cancelCurrentTransaction();
        waitForCancel();
    }

    public interface ICancelTransaction {
        void cancelTransaction();
    }
}
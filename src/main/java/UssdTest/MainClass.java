package UssdTest;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static UssdTest.SendMoney.ANSI_BRIGHT_RED;

// apt install librxtx-java
// sudo chmod 666 /dev/ttyUSB*

public class MainClass {
    public static final String FILE_STATUS_PATH = "./status.txt";
    public static final String FILE_RESULT_PATH = "./result.txt";
    public static final String FILE_COMPORT_PATH = "./comPort.txt";
    public static final String PROVIDER_ORANGE = "orange";
    public static final String PROVIDER_MTN = "mtn";
    public static final String PROVIDER_TEST = "test";
    private static SendMoney sendMoney;
    private static CancelTransaction cancelTransaction;

    public static void main(String[] args) {
        ArrayList<String> parameters = new ArrayList<>();

        /*order of arguments:
         * Provider
         * phone number of customer
         * amount of transaction
         * pin code of merchant
         */

        /*parameters.add(0, args[0]);
        parameters.add(1, args[1]);
        parameters.add(2, args[2]);
        parameters.add(3, args[3]);*/

        parameters.add(0, "test");
        parameters.add(1, "651942436");
        parameters.add(2, "200");
        parameters.add(3, "02000");

        System.out.println("starting transaction");

        sendMoney = new SendMoney(parameters, () -> {
            cancelTransaction.setShouldStop(true);
            cancelTransaction.interrupt();
        });
        cancelTransaction = new CancelTransaction(() -> sendMoney.stopThread());
        sendMoney.start();
        cancelTransaction.start();
    }

    static void writeStatus(String status, String path) {
        try {
            FileWriter myWriter = new FileWriter(path);
            myWriter.write(status);
            myWriter.close();
        } catch (IOException e) {
            System.out.println(ANSI_BRIGHT_RED + "An error occurred when writting status.");
            e.printStackTrace();
        }
    }
}

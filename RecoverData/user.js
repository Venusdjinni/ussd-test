const mongoose = require('mongoose'),
    neoMongoose = require('../services/multi_mongoose').neoMongoose(),
    Schema = mongoose.Schema,
    uniqid = require('uniqid');
const Countries = require("i18n-iso-countries");
const randomString = require('randomstring');

const UserSchema = new Schema({
    kid: {type: String},
    first_name: {type: String},
    last_name: {type: String},
    email: {
        email: {type: String},
        verified: {type: Boolean}
    },
    mobiles:[{
        mobile: {type: String},
        verified: {type: Boolean},
        momo: {type: Boolean}
    }],
    password:{type:String},
    address:{type:String},
    photo:{type:String},
    birth_date:{type: Date},
    registration_date:{type: Number, default:Date.now()},
    activation_date:{type:Number, default:Date.now()},
    balances:[{
        balance: {type: Number},
        frozen: {type: Boolean},
        currency: {type: String}
    }],
    country:{type: String, default:"Cameroon"},
    country_code:{type:String, default:"CM"},
    city:{type:String},
    contacts_list:[{
        contact_name: {type:String},
        contact_mobile:{type:String},
        sbid: {type: String},
        photo: {type: String}
    }],
    account_status: {type: String, enum:["CREATION", "CREATED", "ACTIVATED", "BLOCKED", "STRIKE"], default:"CREATION"},
    account_status_message: {type: String},
    withdrawal_transactions:[{type:mongoose.Schema.Types.ObjectId, ref:'Withdrawals'}],
    funding_transactions:[{type:mongoose.Schema.Types.ObjectId, ref:'Fundings'}],
    payment_transactions:[{type:mongoose.Schema.Types.ObjectId, ref:'Payments'}],
    products_seen: [{type: mongoose.Schema.Types.ObjectId, ref:'Products'}],
    products_wish_list: [{
        product: {type: mongoose.Schema.Types.ObjectId, ref: 'Products'},
        add_time: {type: Number}
    }],
    kiosks_visited: [{type: mongoose.Schema.Types.ObjectId, ref:'Kiosks'}],
    qr_code_identifier: {type: String},
    lang:{type: String},
    devices:[{
        device: {type: String},
        type: {type: String, enum: ["mobile", "web"]},
        connected: {type: Boolean, default: true},
        firebase_token: {type: String},
        lang:{type: String},
        push_notifications_shown_count: {type: String, default: 0},
        last_seen: {type: Number, default: 0}
    }],
    old_passwords:[{type: String}],
    sbid: {type: String},
    cloudinary_photo_pid: {type: String},
    firebase_token: {type: String},
    last_seen: {type: Number},
    invite_link: {
        lid: {type: String},
        link: {type: String},
        dynamic_link: {type: String}
    },
    zip_code: {type: String},
    mpp_id: {type: String},
    identification_piece: {
        passport: {
            url: {type: String},
            pid: {type: String},
            face_url: {type: String},
            face_pid: {type: String}
        },
        residence_permit:{
            recto: {type: String},
            recto_pid: {type: String},
            verso: {type: String},
            verso_pid: {type: String}
        },
        cni:{
            recto: {type: String},
            recto_pid: {type: String},
            verso: {type: String},
            verso_pid: {type: String}
        },
        rib:{
            url: {type: String},
            pid: {type: String}
        },
        proof_of_residence:{
            url: {type: String},
            pid: {type: String}
        },
        verified: {type: Boolean},
        status: {type: String},
        status_code: {type: Number},
        last_update_date: {type: Number},
        verification_count: {type: Number},
        last_verification_date: {type: Number}
    },
    kyc: {
        img: {
            passport: {type: String},
            passport_face: {type: String},
            residence_permit_recto: {type: String},
            residence_permit_verso: {type: String},
            cni_recto: {type: String},
            cni_verso: {type: String},
            proof_of_residence: {type: String}
        },
        pid: {
            passport: {type: String},
            passport_face: {type: String},
            residence_permit_recto: {type: String},
            residence_permit_verso: {type: String},
            cni_recto: {type: String},
            cni_verso: {type: String},
            proof_of_residence: {type: String}
        },
        status: {type: String},
        status_code: {type: Number},
        last_update_date: {type: Number},
        last_verification_date: {type: Number},
        verification_count: {type: Number},
        data: {
            real_first_name: {type: String},
            real_last_name: {type: String},
            kyc_country: {type: String},
            kyc_city: {type: String},
            kyc_address: {type: String},
            kyc_zip_code: {type: String}
        }
    },
    rib: {
        img_url: {type: String},
        img_pid: {type: String},
        status: {type: String},
        last_update_date: {type: Number},
        last_verification_date: {type: Number},
        verification_count: {type: Number},
        data: {
            iban: {type: String},
            bic: {type: String}
        }
    },
    mobile_app_secret_token: {type: String},
    bank_info: {
        first_name: {type: String},
        last_name: {type: String},
        iban: {type: String},
        bic: {type: String}
    },
    eth_address: {type: String}
});

UserSchema.virtual('id').get(()=>{
    return this==null?null:this._id;
});

UserSchema.methods.getName = function(){
    if (this.first_name == null && this.last_name == null) return null;
    else if (this.first_name == null) return this.last_name;
    else if (this.last_name == null) return this.first_name;
    return this.first_name + " " + this.last_name;
};

UserSchema.methods.getRealName = function(){
    let name = null;
    if (this.kyc!=null && this.kyc.data!=null){
        if (this.kyc.data.real_first_name!=null && this.kyc.data.real_last_name!=null)
            name = this.kyc.data.real_first_name + " " + this.kyc.data.real_last_name;
        else if (this.kyc.data.real_first_name!=null) name = this.kyc.data.real_first_name;
        else if (this.kyc.data.real_last_name!=null) name = this.kyc.data.real_last_name;
    }
    if (name==null && this.bank_info!=null){
        if (this.bank_info.first_name!=null && this.bank_info.last_name!=null)
            name = this.bank_info.first_name + " " + this.bank_info.last_name;
        else if (this.bank_info.first_name!=null) name = this.bank_info.first_name;
        else if (this.bank_info.last_name!=null) name = this.bank_info.last_name;
    }
    return name;
}

UserSchema.methods.getNameOrMobile = function(){
    if ((this.first_name == null ||this.first_name==="") && (this.last_name == null || this.last_name==="")) {
        if (this.mobiles==null || this.mobiles.length===0) return null;
        else return this.mobiles[0].mobile;
    }
    else if (this.last_name == null || this.last_name==="") return this.first_name;
    else if (this.first_name == null || this.first_name==="") return this.last_name;
    return this.first_name + " " + this.last_name;
};

UserSchema.methods.getRealNameOrMobile = function(){
    let name = null;
    if (this.kyc!=null && this.kyc.data!=null){
        if (this.kyc.data.real_first_name!=null && this.kyc.data.real_last_name!=null)
            name = this.kyc.data.real_first_name + " " + this.kyc.data.real_last_name;
        else if (this.kyc.data.real_first_name!=null) name = this.kyc.data.real_first_name;
        else if (this.kyc.data.real_last_name!=null) name = this.kyc.data.real_last_name;
    }
    if (name==null && this.bank_info!=null){
        if (this.bank_info.first_name!=null && this.bank_info.last_name!=null)
            name = this.bank_info.first_name + " " + this.bank_info.last_name;
        else if (this.bank_info.first_name!=null) name = this.bank_info.first_name;
        else if (this.bank_info.last_name!=null) name = this.bank_info.last_name;
    }
    if (name == null){
        if ((this.first_name == null ||this.first_name==="") && (this.last_name == null || this.last_name==="")) {
            if (this.mobiles!=null && this.mobiles.length>0) name = this.mobiles[0].mobile;
        }
        else if (this.last_name == null || this.last_name==="") name = this.first_name;
        else if (this.first_name == null || this.first_name==="") name = this.last_name;
        else name = this.first_name + " " + this.last_name;
    }
    return name;
};

UserSchema.methods.getNameOrMoMoMobile = function(){
    if ((this.first_name == null ||this.first_name==="") && (this.last_name == null || this.last_name==="")) {
        return this.momoMobile();
    }
    else if (this.last_name == null || this.last_name==="") return this.first_name;
    else if (this.first_name == null || this.first_name==="") return this.last_name;
    return this.first_name + " " + this.last_name;
};

UserSchema.methods.mainMobile = function(){
    if (this.mobiles==null || this.mobiles.length===0) return null;
    else return this.mobiles[0].mobile;
};

UserSchema.methods.mainMobileVerified = function(){
    if (this.mobiles==null || this.mobiles.length===0) return false;
    else return this.mobiles[0].verified;
};

UserSchema.methods.momoMobile = function(){
    if (this.mobiles==null || this.mobiles.length===0) return null;
    else {
        for (let i=0; i<this.mobiles.length; i++){
            if (this.mobiles[i].momo === true) return this.mobiles[i].mobile;
        }
        return null;
    }
};

UserSchema.methods.isEligibleForReceiveMoney = function(){
    return this.momoMobile()!=null;
};

UserSchema.methods.getBalance = function(currency){
    for (let i=0; i<this.balances.length; i++){
        if (this.balances[i].currency===currency) return i;
    }
    return -1;
};

UserSchema.methods.checkBalance = function(amount, currency){
    for (let i=0; i<this.balances.length; i++){
        if (this.balances[i].currency===currency) {
            return amount < this.balances[i].balance + 50;
        }
    }
    return false;
};

UserSchema.methods.canAcceptTransaction = function(currency){
    for (let i=0; i<this.balances.length; i++){
        if (this.balances[i].currency===currency) return true;
    }
    return false;
};

UserSchema.methods.getCurrencies = function(){
    let currencies = [];
    for (let i=0; i<this.balances.length; i++){
        currencies.push(this.balances[i].currency);
    }
    return currencies;
};

UserSchema.methods.getCountry = function(lang){
    if (this.country==null || this.country==="") return null;
    if (this.country.length===2) {
        return Countries.getName(this.country, lang);
    }
    else{
        let iso = Countries.getAlpha2Code(this.country, 'en');
        if (iso==null) iso = Countries.getAlpha2Code(this.country, 'fr');
        return Countries.getName(iso, lang);
    }
};

UserSchema.methods.isInSEPAZone = function(){
    if (this.country==null || this.country==="") return false;
    let iso = this.country;
    if (iso.length !== 2){
        iso = Countries.getAlpha2Code(this.country, 'en');
        if (iso==null) iso = Countries.getAlpha2Code(this.country, 'fr');
    }
    let SEPA = ["AT", "BE", "CY", "EE", "FI", "AX",
        "FR", "GF", "GP", "MQ", "YT", "BL", "MF", "RE", "PM", "DE", "GR", "IE", "IT",
        "LV", "LT", "LU", "MT", "NL",
        "PT", "SK", "SI", "ES", "PT-20", "PT-30", "IC", "ES-CE", "ES-ML",
        "BG", "HR", "CZ", "DK", "HU", "PL", "RO", "SE", "GB", "GI",
        "IS", "LI", "NO", "MC", "CH", "SM"];
    return SEPA.includes(iso);
};

const twoDigitsDate = function(val){
    let v = val.toString();
    if (v.length===1) v = "0"+v;
    return v;
};

UserSchema.methods.reverseRegistrationDate = function(){
    let rd = new Date();
    rd.setTime(this.registration_date);
    return "A"+rd.getFullYear()+twoDigitsDate(rd.getDate())+twoDigitsDate(rd.getMonth()+1)+"Z"+twoDigitsDate(rd.getHours())+twoDigitsDate(rd.getMinutes())+twoDigitsDate(rd.getSeconds());
};

UserSchema.statics.generateMobileAppSecretToken = function(){
    return randomString.generate(64);
};

UserSchema.methods.langForSMS = function () {
    if (this==null) return "en";
    if (this.devices==null || this.devices.length===0) return this.lang==null?"en":this.lang;
    else{
        let i = 0, flag = false;
        while (i<this.devices.length && !flag){
            if (this.devices[i].connected) flag = true;
            else i++;
        }
        if (flag) return this.devices[i].lang==null?"en":this.devices[i].lang.toString();
        else return "en";
    }
};

UserSchema.methods.getAddress = function (type, lang){
    let add = "";
    if (this.address!=null) add = this.address + ", ";
    if (type!=="small"){
        if (this.city!=null) add = add + this.city + ", ";
        let cnt = this.getCountry(lang);
        if (type==="full" && cnt!=null){
            add = add + cnt + ", ";
        }
    }
    if (add.endsWith(", ")) add = add.substring(0, add.length-2);
    return add;
}

let User = neoMongoose.model('Users', UserSchema);
module.exports = User;
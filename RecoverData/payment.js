const mongoose = require('mongoose'),
    neoMongoose = require('../services/multi_mongoose').neoMongoose(),
    Schema = mongoose.Schema;

const PaymentSchema = new Schema({
    transaction_code: {type: String, required: true},
    sender: {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
    receiver: {type: mongoose.Schema.Types.ObjectId, ref: 'Users'},
    amount: {type: Number},
    fees: {type: Number},
    amount_eq: {type: Number},
    currency: {type: String},
    support_currency: {type: String},
    payment_mode: {type: String, enum: ["CREDIT_CARD", "KAMIX_CARD", "SEPA_MANDATE", "KAMIX_SOLD"]},
    initialization_date: {type: Number, default: Date.now()},
    max_date: {type:Number},
    completion_date: {type: Number},
    status: {type: String, enum:["INITIALIZED", "REQUESTED", "TO_NON_KAMIX_PENDING", "CONFIRMED", "PENDING", "PAID", "DONE", "CANCELLED", "REFUSED", "FAILED"]},
    context: {type: String},
    type: {type: String, enum:["TRANSFER", "VIA_LINK", "QR_CODE_PAYMENT", "TRANSFER_TO_NON_KAMIX_USER", "FUND"]},
    qr_code: {type: String},
    link: {type: mongoose.Schema.Types.ObjectId, ref:'Payment Links'},
    transfer_receiver_mobile: {type: String},
    transfer_request_sender_mobile: {type: String},
    transfer_initialized_by: {type: String, enum:["SENDER", "RECEIVER"]},
    transfer_receiver_name: {type: String},
    transfer_password: {type: String},
    easy_transac: {
        tid: {type: String},
        req_id: {type: String},
        status: {type: String}
    },
    lydia: {
        tid: {type: String},
        uuid: {type: String},
        order_ref: {type: String},
        status : {type: String},
        payment_url: {type: String}
    },
    auto_momo : {
        tid: {type: String},
        status: {type: String}
    },
    sepa_mandate : {
        mandate: {type: mongoose.Schema.Types.ObjectId, ref: 'SEPAMandates'},
        debited: {type: Boolean, default: false},
        rejected: {type: Boolean, default: false},
    },
    kamix_card: {
        cid: {type: String}
    },
    eth_tx_fund_hash: {type: String},
    eth_tx_send_hash: {type: String},
    eth_tx_withdraw_hash: {type: String}
});

PaymentSchema.virtual('id').get(()=>{
    return this==null?null:this._id;
});

let Payment = neoMongoose.model('Payments', PaymentSchema);
module.exports = Payment;